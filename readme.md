# Java Program Control
A project that illustrates the use of the following:
- Variables
- Scope
- If Else
- Switch
- Loops
- Functions

# Notes
Built using ItelliJ IDEA 2021.2.2 (Community Edition)
