package ProgramControl.Enums;

public enum DaysOfWeek { // Enumerated Constant.
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}
