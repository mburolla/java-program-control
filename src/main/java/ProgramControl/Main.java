package ProgramControl;

public class Main {
    public static void main(String[] args) {
        Worker worker = new Worker();
//        worker.variables();
//        worker.scope();
//        worker.ifElse();
//        worker.switchKeyword();
//        worker.loops();
//        worker.equalityTest();
//        worker.functions();
//        worker.processInput();
//        worker.optionals();
        worker.enums();
    }
}
